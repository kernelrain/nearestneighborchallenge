#include <stdlib.h>
#include <float.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#include "graph.h"


float **Graph_AllocDistanceTable(int rows, int cols);
void Graph_GenerateDistanceTable(Graph *graph);


/*
 * Loads a Graph consisting of `nodeCount` nodes from the file specified by `input`.
 * Also, a distance table is being allocated.
 * A pointer to the `Graph` is being returned.
 * Note: As memory is being allocated by this function,
 * for each call of `Graph_Load` there should be a call to `Graph_Unload` (see below).
 */
Graph *Graph_Load(FILE *input, int nodeCount)
{
    // Allocate graph
    Graph *graph = malloc(sizeof(Graph));
    // Allocate node array with `nodeCount` elements
    graph->nodes = malloc(sizeof(Node) * nodeCount);
    // Save `nodeCount` to graph
    graph->count = nodeCount;
    // Read `nodeCount` lines and append them to the `nodes` array
    for (int i = 0; i < nodeCount; i++)
    {
        Node *currentNode = &graph->nodes[i];
        currentNode->id = i;
        fscanf(input, "%s %f %f\n",
               (char *)&currentNode->code,
               &currentNode->latitude,
               &currentNode->longitude);
    }

    // Allocate the distance table
    graph->distanceTable = Graph_AllocDistanceTable(nodeCount, nodeCount);
    // And also generate it
    Graph_GenerateDistanceTable(graph);

    return graph;
}

/*
 * Takes a graph and an id of the 'center' (which is equal to the index of a node in the `nodes` array of the graph).
 * Computes and returns the id of the node closest to the node specified by using the graph's distance table.
 */
int Graph_FindNearestNode(Graph *graph, int id)
{
    // Start with a maximal distance
    float minDistance = FLT_MAX;
    int closestNode = -1;
    // Iterate over all nodes, calculating the distance between the center node and the current node
    // Save result
    for (int i = 0; i < graph->count; i++)
    {
        // Skip center node
        if (i == id)
            continue;
        float distance = graph->distanceTable[id][i];
        if (distance < minDistance)
        {
            minDistance = distance;
            closestNode = i;
        }
    }
    // And return it
    return closestNode;
}

/*
 * Takes a `Graph` and a string `code` containing the region code of a node.
 * Returns the id of that node.
 */
int Graph_FindNodeByCode(Graph *graph, char *code)
{
    for (int i = 0; i < graph->count; i++)
        if (strcmp(graph->nodes[i].code, code) == 0)
            return i;
    return -1;
}

/*
 * Makes a selection of 20 nodes out of the `graph`'s nodes and saves them to the int array `nodes`
 */
void Graph_SelectNodes(Graph *graph, int *nodes)
{
    for (int i = 0; i < 20; i++)
        nodes[i] = rand() % graph->count;
}

/*
 * Generates the distance table for the given graph.
 */
void Graph_GenerateDistanceTable(Graph *graph)
{
    for (int i = 0; i < graph->count; i++)
        for (int j = 0; j < graph->count; j++)
        {
            // Calculate the 2-dimensional euclidian distance between graph->nodes[i] and graph->nodes[j] ...
            float dx = graph->nodes[i].longitude - graph->nodes[j].longitude;
            float dy = graph->nodes[i].latitude - graph->nodes[j].latitude;
            float distance = (float)sqrt(dx * dx + dy * dy);
            // ... and save it to the graphs distance table
            graph->distanceTable[i][j] = distance;
        }
}

/*
 * Safely deallocates the memory allocated by a Graph.
 * This function is supposed to be used to prevent memory leaks.
 */
void Graph_Unload(Graph *graph)
{
    free(graph->nodes);
    for (int i = 0; i < graph->count; i++)
        free(graph->distanceTable[i]);
    free(graph->distanceTable);
    free(graph);
}

/*
 * Allocates a 2-dimensional float array containing `rows` rows and `cols` columns.
 * Returns a reference to that float array.
 */
float **Graph_AllocDistanceTable(int rows, int cols)
{
    float **table = malloc(sizeof(float *) * rows);
    for (int i = 0; i < rows; i++)
        table[i] = malloc(sizeof(float) * cols);
    return table;
}

/*
 * Outputs a human-readable representation of given node on the command line.
 */
void Graph_PrintNode(Node *node)
{
    printf("Node(Code=%-8s, Latitude=%.5f, Longitude=%.5f)\n",
            node->code,
            node->latitude,
            node->longitude);
}

/*
 * Prints `nNodes` nodes specified by their id in `nodes` in a human-readable, table-like formatted structure.
 */
void Graph_PrintNodes(Graph *graph, int *nodes, int nNodes)
{
    for (int i = 0; i < nNodes; i++)
    {
        Node node = graph->nodes[nodes[i]];
        printf("Code=%-8s | Latitude=%+.5f° | Longitude=%+.5f°\n",
                node.code,
                node.latitude,
                node.longitude);
    }
}

/*
 * Creates a circuit from a `graph` and an array of 20 selected nodes.
 * Returns a pointer to that Circuit.
 */
Circuit *Circuit_From(Graph *graph, int *selectedNodes)
{
    Circuit *circuit = malloc(sizeof(Circuit));
    circuit->graph = graph;
    circuit->selectedNodes = selectedNodes;
    circuit->visited = malloc(sizeof(bool) * 20);
    for (int i = 0; i < 20; i++)
        circuit->visited[i] = false;
    return circuit;
}

/*
 * Deallocates a circuit, in particular the `circuit`'s `visited`-array. and the `circuit` itself.
 * Note: This does NOT deallocate the `selectedNodes`-array as it is externally managed and not 'owned' by the circuit.
 */
void Circuit_Dealloc(Circuit *circuit)
{
    free(circuit->visited);
    free(circuit);
}


/*
 * Finds the closest node within the selection specified by the given circuit.
 * Returns the index of the closest node (IMPORTANT: This index is the index of the closest node WITHIN THE NODE SELECTION, not the absolute index in the graph node array)
 * Arguments:
 * Circuit * describing the portion of the graph to traverse
 * id of the center node (IMPORTANT: id corresponds to the index of the node in the Node[]-Array of the Graph, NOT the index in the selected list!)
 */
int Circuit_FindNearestNode(Circuit *circuit, int id)
{
    float minDistance = FLT_MAX;
    int closestNode = -1;
    for (int i = 0; i < 20; i++)
    {
        // Convert loop index (0..20) to Graph indices by obtaining the graph index from the selectedNodes[] Array
        int nodeIndex = circuit->selectedNodes[i];
        // Continue if node is center node or node was already visited
        if (nodeIndex == id || circuit->visited[i])
            continue;
        // Retrieve distance from center node to current node
        float distance = circuit->graph->distanceTable[id][nodeIndex];
        if (distance < minDistance)
        {
            minDistance = distance;
            closestNode = i;
        }
    }
    return closestNode;
}

/*
 * Calculates a short route through selected nodes using the Nearest Neighbor Algorithm.
 *
 * Arguments:
 * int *route: pointer to an array with sufficient space for at least 20 nodes (int)
 */
void Circuit_Traverse(Circuit *circuit, int *route)
{
    int index = 0;
    // We need to start at the node closest to Main Manchester ("M1_2EA")
    // First, find id of Main Manchester
    int idManchester = Graph_FindNodeByCode(circuit->graph, "M1_2EA");
    assert(idManchester == 723);
    // now, find index of node closest to Main Manchester relative to the selected nodes
    int idClosestManchester = Circuit_FindNearestNode(circuit, idManchester);
    /*
     * Now, execute the nearest neighbor algorithm:
     * 1) Start at the current node U
     * 2) Find the node V that is a) unvisited and b) closest to current node
     * 3) Set V as the new current node
     * 4) Mark V as visited
     * 5) If all nodes are visited, terminate: We have found a solution
     * 6) Otherwise, start over from 2)
     */
    int currentNode = idClosestManchester;
    while (true)
    {
        assert(index <= 20);
        int nextNode = Circuit_FindNearestNode(circuit, currentNode);
        currentNode = nextNode;
        // Append current node to route
        route[index++] = currentNode;
        circuit->visited[currentNode] = true;
        // Test whether all nodes were visited
        bool visitedAll = true;
        for (int i = 0; i < 20; i++)
            visitedAll &= circuit->visited[i];
        if (visitedAll)
            break;
    }
}

/*
 * Maps the indices of the selected nodes (0..19) to the corresponding indices of the Graph's node array.
 * Saves the node-array indices to the int array `nodeIndices`
 */
void Circuit_MapRouteToNodes(Circuit *circuit, int *route, int *nodeIndices)
{
    for (int i = 0; i < 20; i++)
        nodeIndices[i] = circuit->selectedNodes[route[i]];
}
