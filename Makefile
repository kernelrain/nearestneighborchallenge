CC = gcc
LFLAGS = -lm -Wall
CFLAGS = -c -Wall -ggdb
OBJS = main.o graph.o
EXECUTABLE = nearest_neighbor

all: $(OBJS)
	$(CC) $(LFLAGS) $(OBJS) -o $(EXECUTABLE)

main.o: main.c
	$(CC) $(CFLAGS) main.c

graph.o: graph.c graph.h
	$(CC) $(CFLAGS) graph.c

clean:
	rm $(OBJS) $(EXECUTABLE)

run:
	./$(EXECUTABLE)
