#ifndef GRAPH_H
#define GRAPH_H

#include <stdio.h>
#include <stdbool.h>

// Graph
typedef struct {
    int id;
    char code[10];
    float longitude, latitude;
} Node;

typedef struct {
    int count;
    Node *nodes;
    float **distanceTable;
} Graph;

Graph *Graph_Load(FILE *input, int nodeCount);
void Graph_Unload(Graph *graph);
int Graph_FindNearestNode(Graph *graph, int id);
int Graph_FindNodeByCode(Graph *graph, char *code);
void Graph_SelectNodes(Graph *graph, int *nodes);
void Graph_PrintNode(Node *node);
void Graph_PrintNodes(Graph *graph, int *nodes, int nNodes);

// Circuit
typedef struct {
    Graph *graph;
    bool *visited;
    int *selectedNodes;
} Circuit;

Circuit *Circuit_From(Graph *graph, int *selectedNodes);
void Circuit_Dealloc(Circuit *circuit);
void Circuit_Traverse(Circuit *traversal, int *route);
void Circuit_MapRouteToNodes(Circuit *traversal, int *route, int *nodeIndices);

#endif // GRAPH_H
