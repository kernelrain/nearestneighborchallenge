#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "graph.h"

#define pr(x) for(int i = 0; i < 20; i++) printf("%d ", x[i]); puts("")

void printArray(int *array, int len)
{
    printf("[");
    for (int i = 0; i < len; i++)
    {
        printf("%d", array[i]);
        if (i < len - 1)
            printf(", ");
    }
    puts("]");
}

int main(int argc, char **argv)
{
    // Init RNG
    srand(time(NULL));

    // Open postcodes file, exit, if error occurrs
    FILE *input = fopen("postcodes.txt", "r");
    if (input == NULL)
    {
        puts("Error loading file, exiting...");
        return EXIT_FAILURE;
    }

    // Load graph from file
    Graph *graph = Graph_Load(input, 1726);
    // Close input file handle
    fclose(input);

    bool random = true;
    // Select waypoint nodes
    int *selection = malloc(sizeof(int) * 20);
    if (random)
        Graph_SelectNodes(graph, selection);
    else
        for (int i = 0; i < 20; i++)
            selection[i] = 723 + i;

    puts("Selected nodes: ");
    printArray(selection, 20);

    // Construct Circuit Object
    Circuit *circuit = Circuit_From(graph, selection);
    int *route = malloc(sizeof(int) * 20);
    for (int i = 0; i < 20; i++)
        route[i] = -1;

    // Traverse circuit, searching for a short route
    Circuit_Traverse(circuit, route);

    // Map route indices back to node indices
    int *mappedRoute = malloc(sizeof(int) * 20);
    Circuit_MapRouteToNodes(circuit, route, mappedRoute);
    puts("Mapped route");
    printArray(mappedRoute, 20);

    // Output route
    Graph_PrintNodes(graph, mappedRoute, 20);

    // Dealloc graph
    Graph_Unload(graph);
    Circuit_Dealloc(circuit);

    free(selection);
    free(route);
    free(mappedRoute);

    return EXIT_SUCCESS;
}
